# OpenML dataset: World-Happiness-Ranking

https://www.openml.org/d/43594

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The World Happiness Ranking focuses on the social, urban, and natural environment. Specifically, the ranking relies on self-reports from residents of how they weigh the quality of life they are currently experiencing which englobes three main points: current life evaluation, expected future life evaluation, positive and negative affect (emotion). Half of the underlying data comes from multiple Gallup world polls which asked people to give their assessment of the previously mentioned points, and the other half of the data is comprised of six variables that could be used to try to explain the individuals perception in their answers.
Content
The data sources datasets were obtained in two different formats. The World Happiness Ranking Dataset is a Comma-separated Values (CSV) file with multiple columns (for the different variables and the score) and a row for each of the analyzed countries. 
The rankings of national happiness are based on a Cantril ladder survey. Nationally representative samples of respondents are asked to think of a ladder, with the best possible life for them being a 10, and the worst possible life being a 0. They are then asked to rate their own current lives on that 0 to 10 scale. The report correlates the results with various life factors.

GDP per capita is in terms of Purchasing
Power Parity (PPP) adjusted to constant
2011 international dollars, taken from
the World Development Indicators
(WDI) released by the World Bank on
November 28, 2019. See Statistical
Appendix 1 for more details. GDP data
for 2019 are not yet available, so we
extend the GDP time series from 2018
to 2019 using country-specific forecasts
of real GDP growth from the OECD
Economic Outlook No. 106 (Edition
November 2019) and the World Banks
Global Economic Prospects (Last
Updated: 06/04/2019), after adjustment
for population growth. The equation
uses the natural log of GDP per capita,
as this form fits the data significantly
better than GDP per capita.
The time series of healthy life expectancy
at birth are constructed based on data
from the World Health Organization
(WHO) Global Health Observatory data
repository, with data available for 2005,
2010, 2015, and 2016. To match this
reports sample period, interpolation and
extrapolation are used. See Statistical
Appendix 1 for more details.
Social support is the national average of
the binary responses (0=no, 1=yes) to
the Gallup World Poll (GWP) question, If
you were in trouble, do you have relatives
or friends you can count on to help you
whenever you need them, or not?
Freedom to make life choices is the
national average of binary responses
to the GWP question, Are you satisfied
or dissatisfied with your freedom to
choose what you do with your life?
Generosity is the residual of regressing
the national average of GWP responses
to the question, Have you donated
money to a charity in the past month?
on GDP per capita.
Perceptions of corruption are the average
of binary answers to two GWP questions:
Is corruption widespread throughout the
government or not? and Is corruption
widespread within businesses or not?
Where data for government corruption
are missing, the perception of business
corruption is used as the overall
corruption-perception measure.
Positive affect is defined as the average
of previous-day affect measures for
happiness, laughter, and enjoyment for
GWP waves 3-7 (years 2008 to 2012, and
some in 2013). It is defined as the average
of laughter and enjoyment for other
waves where the happiness question was
not asked. The general form for the
affect questions is: Did you experience
the following feelings during a lot of the
day yesterday? See Statistical Appendix
1 for more details.
Negative affect is defined as the average
of previous-day affect measures for
worry, sadness, and anger in all years.

Acknowledgements
The World Happiness Report is a publication of the Sustainable Development Solutions Network, powered by data from the Gallup World Poll, and supported by the Ernesto Illy Foundation, illycaff, Davines Group, Blue Chip Foundation, the William, Jeff, and Jennifer Gross Family Foundation, and Unilevers largest ice cream brand Walls.
Inspiration
Find the relationship between the ladder score and the other pieces of data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43594) of an [OpenML dataset](https://www.openml.org/d/43594). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43594/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43594/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43594/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

